var db = require('./db');

module.exports = {
    gameStartTime: async function(code) {
        try {
            const check = await db.query("SELECT COUNT(*) AS checkGame FROM eo_game WHERE hash_code = ? AND server_start_time IS NULL", [code]);
            if (parseInt(check[0].checkGame) > 0) {
                await db.query("UPDATE eo_game SET server_start_time = NOW(), updated = NOW() WHERE hash_code = ?", [code]);
                console.log("Database start time updated");
            } else {
                console.log('No game!');
            }
        } catch (err) {
            console.log(err);
            console.log('line', 15)
        }
    },

    gameScoreUpdate: async function(data, code) {

        try {
            var game = await db.query("SELECT id, fk_game_code_id, fk_player_id FROM eo_game WHERE hash_code = ?", [code]);

            var score = await db.query("SELECT COUNT(id) AS checkScore, saves, perfect_saves, goals FROM eo_score WHERE fk_game_code_id = ? GROUP BY id", [game[0].fk_game_code_id]);

            var values = [
                game[0].fk_player_id,
                game[0].fk_game_code_id,
                data.score,
                data.level + 1,     // starts from 0 instead of 1
                data.saves,
                data.perfectSaves,
                data.goals,
                data.lives
            ];

            if (typeof score[0] !== 'undefined' && parseInt(score[0].checkScore) > 0) {

                values = [
                    game[0].fk_player_id,
                    game[0].fk_game_code_id,
                    data.score,
                    data.level + 1,
                    data.saves + score[0].saves,
                    data.perfectSaves + score[0].perfect_saves,
                    data.goals + score[0].goals,
                    data.lives,
                    game[0].fk_game_code_id
                ];

                await db.query ("UPDATE eo_score SET fk_player_id = ?, fk_game_code_id = ?, points = ?, level = ?, saves = ?, perfect_saves = ?, goals = ?, lives = ?, updated = now() WHERE fk_game_code_id = ?", values);
                console.log('Score updated!');
            } else {
                await db.query ("INSERT INTO eo_score (fk_player_id, fk_game_code_id, points, level, saves, perfect_saves, goals, lives, created, updated) VALUES (?,?,?,?,?,?,?,?,now(), null)", values);
                console.log('Score inserted!');
            }

        } catch (err) {
            console.log(err, 'line 61');
        }

    },
    gameEndTime: async function(code) {
        try {

            await db.query("UPDATE eo_game SET server_end_time = now() WHERE hash_code = ?", [code]);

            console.log('Database end time updated');

        } catch (err) {
            console.log(err);
            console.log('line:', 33)
        }
    },
}