// http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
"use strict";
require('../assets/node_modules/dotenv').config({ path: '../.env.' + process.env.NODE_ENV })

// Port where we'll run the websocket server
var webSocketsServerPort = process.env.WS_PORT;
var https = require('https');
var fs = require('fs');
// websocket, http servers
var webSocketServer = require('../assets/node_modules/websocket').server;

/**
 * HTTP server
 */
const server = new https.createServer({
    cert: fs.readFileSync(''),
    key: fs.readFileSync(''),
    ca: fs.readFileSync(''),
//  pem: fs.readFileSync('')
});

server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

var wsServer = new webSocketServer({
    httpServer: server
});

module.exports = wsServer;