// http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
"use strict";
require('../assets/node_modules/dotenv').config({ path: '../.env.' + process.env.NODE_ENV })

// Port where we'll run the websocket server
var webSocketsServerPort = process.env.WS_PORT;

// websocket, http servers
var webSocketServer = require('../assets/node_modules/websocket').server;
var http = require('http');

/**
 * HTTP server
 */
var server = http.createServer(function(request, response) {
    // Not important for us. We're writing WebSocket server, not HTTP server
});
server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

var wsServer = new webSocketServer({
    httpServer: server
});

module.exports = wsServer;