// http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
"use strict";

require('../assets/node_modules/dotenv').config({ path: '../.env.' + process.env.NODE_ENV })

var games = new Map();
var hash;
var wsServer;

if (process.env.NODE_ENV == 'local') {
    wsServer = require('./server');
} else {
    wsServer = require('./server-ssl');
}

var func = require('./functions')

wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');

    var connection = request.accept(null, request.origin);

    console.log((new Date()) + ' Connection accepted.');

    connection.on('message', function(message) {
        var msg = message.utf8Data;

        var type = msg.substring(0,5);

       if (type == 'hash-') {
            hash = msg.substring(5);
            games.set(request.key, {hash: hash});
        }

        if (type == 'start' && games.has(request.key)) {
            var game = games.get(request.key);
            func.gameStartTime(game.hash);
        }

        if (type == 'end--' && games.has(request.key)) {
            var game = games.get(request.key);
            func.gameEndTime(game.hash);
        }

        if (msg.charAt(0) == '{') {
            var params = JSON.parse(msg);
            func.gameScoreUpdate(params, hash);
        }

    });

    connection.on('close', function(connection) {
        console.log((new Date()) + " Peer "
            + connection.remoteAddress + " disconnected.");

    });

});