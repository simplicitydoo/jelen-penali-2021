var mysql = require('../assets/node_modules/mysql');
const util = require('util');
var url = require('url');
require('../assets/node_modules/dotenv').config({ path: '../.env.' + process.env.NODE_ENV })

var dbUrl = process.env.DATABASE_URL;

var data = url.parse(dbUrl, true);
var user = data.auth.split(':')


var config = {
    host     : data.hostname,
    port     : data.port,
    user     : user[0],
    password : user[1],
    database : data.pathname.substr(1)
};

function makeDb( config ) {
    const connection = mysql.createConnection( config );
    return {
        query( sql, args ) {
            return util.promisify( connection.query )
                .call( connection, sql, args );
        },
        close() {
            return util.promisify( connection.end ).call( connection );
        }
    };
}

const con = makeDb( config );

module.exports = con;
