<?php

namespace App\Repository;

use App\Entity\GameCode;
use App\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GameCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameCode[]    findAll()
 * @method GameCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameCodeRepository extends ServiceEntityRepository {
  public function __construct(ManagerRegistry $registry) {
    parent::__construct($registry, GameCode::class);
  }

  public function save(string $code, Player $player, int $check, string $ip, string $userAgent, array $device): void {

    $em = $this->getEntityManager();

    $codeNew = new GameCode();

    $codeNew->setPlayer($player);
    $codeNew->setIp($ip);
    $codeNew->setUserAgent($userAgent);
    $codeNew->setIsMobile($device['mobile']);
    $codeNew->setIsTablet($device['tablet']);
    $codeNew->setIsDesktop($device['desktop']);
    $codeNew->setCode($code);
    $codeNew->setIsUsed(true);

    if ($code == "56321454") {
      $codeNew->setIsTest(true);
    } else {
      $codeNew->setIsTest(false);
    }

    if ($check == 1) {
      $codeNew->setIsValid(true);
    } else {
      $codeNew->setIsValid(false);
    }

    $em->persist($codeNew);
    $em->flush();
  }

  public function cifra1(string $osnova): int {
    $cifra1 = 0;
    $br = 0;
    for ($i = 0; $i < 13; $i++) {
      $br = $br + 1;
      if ($br % 2 == 0) {
        $cifra1 = $cifra1 + intval(substr($osnova, $i, 1));
      } else {
        $cifra1 = $cifra1 + intval(substr($osnova, $i, 1)) * 3;
      }
    }

    $cifra1 = (10 - $cifra1 % 10) % 10;

    return $cifra1;
  }

  public function cifra2(string $osnova): int {
    $cifra2 = 0;
    $br = 0;
    for ($i = 0; $i < 14; $i++) {
      $br = $br + 1;
      if ($br % 2 == 0) {
        $cifra2 = $cifra2 + intval(substr($osnova, $i, 1)) * 3;
      } else {
        $cifra2 = $cifra2 + intval(substr($osnova, $i, 1));
      }
    }
    $cifra2 = (10 - $cifra2 % 10) % 10;

    return $cifra2;
  }

  //  poslovnica | objekat | kasa | dan u mesecu | kontrola | broj racuna
  //kod = 4 012 07 05 28 0030
  //kod = 40120705280030
  //cifra1 = 2
  //cifra2 = 8
  public function checkCode(string $code): int {
    if ($code == '56321454') {
      return 1;
    }

    $check = 0;
    $osnova =
      substr($code, 0, 1) .
      substr($code, 1, 3) .
      substr($code, 4, 2) .
      substr($code, 6, 2) .
      substr($code, 10, 4);

    $cifra1 = $this->cifra1($osnova);
    $cifra2 = $this->cifra2($osnova . strval($cifra1));

    $em = $this->getEntityManager();
    $codeObject = $em->getRepository(GameCode::class)->findOneBy(['code' => $code]);

    if ($codeObject) {
      $check = -1;
    }


    if (strlen($code) == 14) {
      if (!$codeObject) {
        if ($cifra1 == intval(substr($code, 8, 1)) && $cifra2 == intval(substr($code, 9, 1))) {
          $check = 1;
        }
      }
    }

    return $check;
  }

  // /**
  //  * @return GameCodeEntity[] Returns an array of GameCodeEntity objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('g')
          ->andWhere('g.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('g.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */

  /*
  public function findOneBySomeField($value): ?GameCodeEntity
  {
      return $this->createQueryBuilder('g')
          ->andWhere('g.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
}
