<?php

namespace App\Repository;

use App\Classes\Helper;
use App\Entity\Game;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository {
  public function __construct(ManagerRegistry $registry) {
    parent::__construct($registry, Game::class);
  }

  public function newGame($player, $code, $device, $ip, $lives, $level = 1, $points = 0): Game {
    $em = $this->getEntityManager();

    $Game = new Game();
    $Game->setPlayer($player);
    $Game->setGameCode($code);
    $Game->setIsMobile($device['mobile']);
    $Game->setIsTablet($device['tablet']);
    $Game->setIsDesktop($device['desktop']);
    $Game->setIp($ip);
    $Game->setIsCodeValid($code->getIsValid());
    $Game->setLives($lives);
    $Game->setLevel($level);
    $Game->setPoints($points);
    $Game->setHashCode(Helper::getRandomString(32));

    try {
      $em->persist($Game);
      $em->flush();
    } catch (\Exception $e) {
    }


    return $Game;
  }

  public function updateGame($game): void {
    $em = $this->getEntityManager();

    $game->setUpdated(new \DateTime());

    $em->flush();
  }

  // /**
  //  * @return GameEntity[] Returns an array of GameEntity objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('g')
          ->andWhere('g.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('g.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */

  /*
  public function findOneBySomeField($value): ?GameEntity
  {
      return $this->createQueryBuilder('g')
          ->andWhere('g.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
}
