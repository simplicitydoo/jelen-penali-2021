<?php

namespace App\Service;

use Doctrine\DBAL\Driver\Connection;
use Psr\Log\LoggerInterface;

class CodesService {

  const UPLOAD_DIR = 'uploads/codes/';
  private $db;
  private $logger;

  public function __construct(Connection $db, LoggerInterface $logger) {
    $this->db = $db;
    $this->logger = $logger;
  }

  public function save($file): array {

    $path = $file->getPathName();
    $args = [];
    if (($handle = fopen($path, 'r')) !== false) {
      while (($data = fgetcsv($handle, 1000, ',')) !== false) {
        try {
          $code = $this->getData($data);
          $Code = $this->getCode($code);
          if (!$Code && !empty($code)) {
            $this->insert($code);
          }
          $args['success'] = 1;
        } catch (\Exception $e) {
          $this->logger->error('save error:: ', (array)$e->getMessage());
          $args['success'] = -1;
        }
      }
      fclose($handle);
    }

    $args['fileName'] = $this->upload($file);
    $this->saveFileNameToCronTable($args['fileName']);     

    return $args;
  }

  private function getData($data) {
    $c = implode($data);
    $code = str_replace('.000', '', $c);
    return $code;
  }

  private function insert($code) {
    $time = new \DateTime();
    $sql = 'INSERT INTO eo_code (code, created, is_used) VALUES (:code, :created, :used)';
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':code', $code);
    $stmt->bindValue(':created', $time->format('Y-m-d H:i:s'));
    $stmt->bindValue(':used', 0);
    $stmt->execute();

    return true;
  }

  private function getCode($code) {
    $sql = 'SELECT id, code FROM eo_code WHERE code = :code';
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':code', $code);
    $stmt->execute();

    return $stmt->fetch();
  } 

  public function activate() {
    $codes = $this->getCodes();

    $this->updateValid($codes);
    $this->updateUsed();
    $this->updateGame();
    $this->updateFirstCronFinished();
  }

  private function getCodes() {
    $sql = 'SELECT id, code FROM eo_code WHERE is_used = 0';
    $stmt = $this->db->prepare($sql);
    $stmt->execute();

    $result = [];
    while ($data = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $result[$data['id']] = $data['code'];
    }

    return $result;
  }

  private function updateValid($codes) {
    $c = array_map([$this, 'substringCode'], $codes);
    $codes = implode(",", $c);

    $sql = "UPDATE eo_game_code SET is_valid = :isValid WHERE LEFT(code, 17) IN ($codes)";
    $stmt = $this->db->prepare($sql);
    $stmt->bindValue(':isValid', true);
    $stmt->execute();
    
  }

  private function updateUsed() {
    $time = new \DateTime();

    $sql = 'UPDATE eo_code c 
            JOIN eo_game_code gc ON LEFT(gc.code, 17) = LEFT(c.code, 17) 
            SET c.is_used = :isUsed, c.used = :used, c.fk_user_id = gc.fk_user_id';
    
    $stmt = $this->db->prepare($sql);
    $stmt->bindValue(':isUsed', true);
    $stmt->bindValue(':used', $time->format('Y-m-d H:i:s'));
    $stmt->execute();
  }

  private function updateGame() {
    $sql = 'UPDATE eo_game AS g, eo_game_code AS gc 
            SET g.is_code_valid = 1
            WHERE g.fk_game_code_id = gc.id AND gc.is_valid = 1';
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
  }

  public function upload($file)
  {
    $fileName = (new \DateTime())->format('YmdHis') . '_' . $file->getClientOriginalName();

    $file->move(self::UPLOAD_DIR, $fileName);

    return $fileName;
  }

  private function saveFileNameToCronTable($fileName) 
  {
    $sql = 'INSERT INTO cron_table (file_name) VALUES (:nameOfFile)';
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':nameOfFile', $fileName);
    $stmt->execute();

  }

  private function updateFirstCronFinished() 
  {
    $sql = 'UPDATE cron_table SET first_cron_completed = 1';
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
  }

  private function substringCode($code) {
    return substr($code, 0, 17);
  }

}