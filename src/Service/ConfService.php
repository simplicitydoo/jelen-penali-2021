<?php

namespace App\Service;

class ConfService {

  private $endDate;

  function __construct($endDate) {
    $this->endDate = \DateTime::createFromFormat('Y-m-d H:i', $endDate);
  }

  public function isFinished() {
    return (new \DateTime() > $this->endDate ? 1 : 0);
  }

}