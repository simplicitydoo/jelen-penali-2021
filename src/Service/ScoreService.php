<?php

namespace App\Service;

use Doctrine\DBAL\Driver\Connection;

class ScoreService {
  
  private $db;

  public function __construct(Connection $db) {
    $this->db = $db;
  }

  private function getAllPlayers() {
    $sql = 'SELECT id FROM players';
    $stmt = $this->db->prepare($sql);
    $stmt->execute();

    $result = [];
    while ($data = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $result[] = $data['id'];
    }

    return $result;
  }

  private function getBestScore($userId) { 
    $sql = 'SELECT `id`, `fk_player_id`, `fk_game_code_id`, `points`, `level`, `lives`, `level_points`, `lives`, `saves`, `perfect_saves`, `goals`
            FROM eo_game 
            WHERE fk_player_id = :userId AND is_code_valid = 1 AND level_points IS NOT NULL
            ORDER BY points DESC, `level` DESC 
            LIMIT 1';
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':userId', $userId);
    $stmt->execute();

    return $stmt->fetch();    
  }

  private function insertBestScore($data) {

    $time = new \DateTime();
    $sql = 'INSERT INTO eo_score (fk_player_id, fk_game_code_id, created, points, `level`, fk_game_id, lives, saves, perfect_saves, goals) 
            VALUES (:userId, :gameCodeId, :created, :points, :lvl, :gameId, :lives, :saves, :perfectSaves, :goals)';
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':userId', $data['fk_user_id']);
    $stmt->bindParam(':gameCodeId', $data['fk_game_code_id']);
    $stmt->bindValue(':created', $time->format('Y-m-d H:i:s'));
    $stmt->bindParam(':points', $data['points']);
    $stmt->bindParam(':lvl', $data['level']);
    $stmt->bindParam(':gameId', $data['id']);
    $stmt->bindParam(':lives', $data['lives']);
    $stmt->bindParam(':saves', $data['saves']);
    $stmt->bindParam(':perfectSaves', $data['perfect_saves']);
    $stmt->bindParam(':goals', $data['goals']);
    $stmt->execute();
  }

  public function updateBestScore($data) {
    $time = new \DateTime();

    $sql = 'UPDATE eo_score 
            SET fk_game_code_id = :gameCodeId, created = :created, points = :points, `level` = :lvl, fk_game_id = :gameId, lives = :lives, saves = :saves, perfect_saves = :perfectSaves, goals = :goals
            WHERE fk_player_id = :userId';
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':userId', $data['fk_user_id']);
    $stmt->bindParam(':gameCodeId', $data['fk_game_code_id']);
    $stmt->bindValue(':created', $time->format('Y-m-d H:i:s'));
    $stmt->bindParam(':points', $data['points']);
    $stmt->bindParam(':lvl', $data['level']);
    $stmt->bindParam(':gameId', $data['id']);
    $stmt->bindParam(':lives', $data['lives']);
    $stmt->bindParam(':saves', $data['saves']);
    $stmt->bindParam(':perfectSaves', $data['perfect_saves']);
    $stmt->bindParam(':goals', $data['goals']);
    $stmt->execute();
  }

  public function checkPlayer($playerId) {
    $sql = 'SELECT fk_player_id, points, `level` FROM eo_score WHERE fk_player_id = :playerId';
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':playerId', $playerId);
    $stmt->execute();
    $result = $stmt->fetch();

    return $result;
  }

  public function insert() {
    $usersId = $this->getAllPlayers();
    $cronData = $this->getCronData();
  
    if ($cronData['first_cron_completed']) {
      foreach($usersId as $userId) {
        $user = $this->checkPlayer($userId);
        $data = $this->getBestScore($userId);

        if (!$user && $data) {
          $this->insertBestScore($data);
        } else if($data) {
          if($data['points'] > $user['points'] || ($data['level'] > $user['level'] && $data['points'] == $user['points'])) {
            $this->updateBestScore($data);
          }
        }
      }
      $this->deleteCronData();
    }
  }

  private function getCronData() {
    $sql = 'SELECT `id`, `file_name`, `first_cron_completed` FROM cron_table';
    $stmt = $this->db->prepare($sql);
    $stmt->execute();

    return $stmt->fetch();
  }

  private function deleteCronData() {
    $sql = 'DELETE FROM cron_table';
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
  }


  public function getScores() {
    $sql = "SELECT    s.fk_player_id, s.best_score
            FROM      (
                         SELECT fk_player_id, MAX(points) best_score
                         FROM eo_score
                         GROUP BY fk_player_id
                      ) s

            GROUP BY s.fk_player_id
            ORDER BY s.best_score DESC";

    $stmt = $this->db->prepare($sql);
    $stmt->execute();

    while ($data = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $result[] = $data;
    }
    return $result;
  }

}