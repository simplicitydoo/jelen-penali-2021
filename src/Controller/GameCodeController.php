<?php

namespace App\Controller;

use App\Entity\Player;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\ConfService;

class GameCodeController extends AbstractController {

  /**
   * @Route("/kod", name="jelen_gamecode_index", options={"expose"=true})
   * @param Request $request
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function index(Request $request, Session $session) {
    $GameConf = new ConfService($_ENV['END_GAME']);

    $args = [];

//    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
//      return $this->redirectToRoute('jelen_default_login', [], 301);
//    }

    if (is_null($session->get('playerId'))) {
      return $this->redirectToRoute('jelen_default_login', [], 301);
    }

    if ($GameConf->isFinished()) {
      return $this->redirectToRoute('jelen_default_index', [], 301);
    }

    $args['player'] = $this->getDoctrine()->getRepository(Player::class)->find($session->get('playerId'));

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    if ($request->query->has('error')) {
      $args['error'] = $request->query->get('error');
    }

    return $this->render('GameCode/index.html.twig', $args);
  }
}
