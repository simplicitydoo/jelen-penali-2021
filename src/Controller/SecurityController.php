<?php

namespace App\Controller;

use App\Entity\Player;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\LoginForm;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController {

  /**
   * @var AuthenticationUtils
   */
  private $authenticationUtils;

  public function __construct(AuthenticationUtils $authenticationUtils) {
    $this->authenticationUtils = $authenticationUtils;
  }

  /**
   * @Route("/admin/login", name="admin_login")
   */
  public function loginAction(): Response {
    $form = $this->createForm(LoginForm::class, [
      'email' => $this->authenticationUtils->getLastUsername()
    ]);

    return $this->render('Admin/security/login.html.twig', [
      'last_username' => $this->authenticationUtils->getLastUsername(),
      'form' => $form->createView(),
      'error' => $this->authenticationUtils->getLastAuthenticationError(),
    ]);
  }

  /**
   * @Route("/admin/logout", name="admin_logout")
   */
  public function logoutAction(): void {
  }

}
