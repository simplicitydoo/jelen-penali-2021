<?php

namespace App\Controller;

use App\Entity\Score;
use App\Service\ScoreService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class ScoreController extends AbstractController {

  /**
   * @Route("/rang-lista", name="jelen_score_index", options={"expose"=true})
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function index(Session $session, ScoreService $scoreService) {
    $em = $this->getDoctrine()->getManager();

    $args = [];

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    $args['scores'] = $scoreService->getScores();


//    $args['scores'] = $em->getRepository(Score::class)->findBy([], ['points' => 'DESC', 'level' => 'DESC', 'lives' => 'DESC', 'perfectSaves' => 'DESC', 'saves' => 'DESC', 'goals' => 'ASC' ]);



    return $this->render('Scores/index.html.twig', $args);
  }

}
