<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\GameCode;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\ConfService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @Route("/igra")
 */
class GameController extends AbstractController {

  /**
   * @Route("/", name="jelen_game_index")
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function index(Session $session) {
    $em = $this->getDoctrine()->getManager();
    $GameConf = new ConfService($_ENV['END_GAME']);

    $args = [];
    
//    if (!$authChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
//      return $this->redirectToRoute('jelen_default_login', [], 301);
//    }

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    if ($GameConf->isFinished()) {
      return $this->redirectToRoute('jelen_default_index', [], 301);
    }

    if (!$session->has('gameCode')) {
      return $this->redirectToRoute('jelen_gamecode_index', $args, 301);
    }

    $gameCode = $em->getRepository(GameCode::class)->findOneBy( ['code' => $session->get('gameCode')] );

    $Game = $em->getRepository(Game::class)->findOneBy(['gameCode' => $gameCode]);

    if ($Game && $gameCode->getIsTest() == false) {
      $session->remove('gameCode');
      return $this->redirectToRoute('jelen_gamecode_index', $args, 301);
    }

    return $this->render('Game/index.html.twig', $args);
  }
}
