<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController {
  
  /**
   * @Route("/registracija", name="jelen_user_index")
   */
  public function index(Session $session) {
    $args = [];

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    return $this->render('Forms/index.html.twig', $args);
  }
}
