<?php

namespace App\Controller;

use App\Entity\Code;
use App\Entity\Game;
use App\Entity\Player;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\RegistrationType;
use App\Entity\GameCode;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/interakcije")
 */
class InterakcijeController extends AbstractController {

  /**
   * @Route("/age-verify", name="jelen_interakcije_age_verify", options={"expose"=true})
   * @param Request $request
   * @param Session $session
   * @return JsonResponse
   */
  public function ageVerify(Request $request, Session $session): JsonResponse {

    $session->set('ageVerified', $request->request->get('ageVerified'));

    return new JsonResponse(['success' => 1]);
  }

  /**
   * @Route("/register", name="jelen_interakcije_register")
   * @param Request $request
   * @return RedirectResponse|Response
   */
  public function register(Request $request) {
    $em = $this->getDoctrine()->getManager();

    $args = [];

    $Player = $em->getRepository(Player::class)->findOneBy(['email' => $request->request->get('registration')['email']]);
    $form = $this->createForm(RegistrationType::class, $Player, ['action' => $this->generateUrl('jelen_interakcije_register')]);

    if (!$Player) {
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {

        $player = new Player();
        $player = $form->getData();
        $player->setPlainPassword($form->get('password')->getData());

        $hashedPass = password_hash($form->get('password')->getData(), PASSWORD_DEFAULT);

        $player->setPassword($hashedPass);
        $player->setAcceptRules(1);
        $player->setNoOfCodes(0);
        $player->setEnabled(1);

        $em->persist($player);
        $em->flush();

        return $this->redirectToRoute('jelen_default_login', [], 301);
      }
    } else {
      $args['registrationError'] = -1;

      return $this->redirectToRoute('jelen_default_registration', $args, 301);
    }

    $args['form'] = $form->createView();

    return $this->render('Forms/registration.html.twig', $args);
  }

  /**
   * @Route("/insert-code", name="jelen_interakcije_insert_code")
   * @param Request $request
   * @param Session $session
   * @return RedirectResponse
   */
  public function insertCode(Request $request, Session $session): RedirectResponse {
    $em = $this->getDoctrine()->getManager();

//    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
//      return $this->redirectToRoute('jelen_default_login', [], 301);
//    }

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    if (is_null($session->get('playerId'))) {
      $args['error'] = true;
      return $this->redirectToRoute('jelen_default_login', $args, 301);
    }

    $args = [];
    
    $player = $this->getDoctrine()->getRepository(Player::class)->find($session->get('playerId'));
    $gameCode = $request->request->get('code');
    $ip = $request->getClientIp();
    $device = $this->getDevice();
    $userAgent = $request->headers->get('User-Agent');

    $check = $this->getDoctrine()->getRepository(GameCode::class)->checkCode($gameCode);
    $this->getDoctrine()->getRepository(GameCode::class)->save($gameCode, $player, $check, $ip, $userAgent, $device);

    if ($check == 1) {

      $session = new Session();
      $session->set('gameCode', $gameCode);
      $session->set('numOfLives', 3);

      return $this->redirectToRoute('jelen_game_index', $args, 301);
    } else if ($check == 0) {
      $args['error'] = -1;
      return $this->redirectToRoute('jelen_gamecode_index', $args, 301);
    } else if ($check == -1) {
      $args['error'] = -2;
      return $this->redirectToRoute('jelen_gamecode_index', $args, 301);
    }
  }

  /**
   * @Route("/game-start", name="jelen_interakcije_game_start", options={"expose"=true})
   * @param Request $request
   * @param Session $session
   * @return JsonResponse|RedirectResponse
   */
  public function gameStart(Request $request, Session $session) {
    $em = $this->getDoctrine()->getManager();

//    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
//      return $this->redirectToRoute('jelen_default_login', [], 301);
//    }

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    if (is_null($session->get('playerId'))) {
      $args['error'] = true;
      return $this->redirectToRoute('jelen_default_login', $args, 301);
    }
    
    $args = [];
    $numOfLives = $session->get('numOfLives');

    if ($numOfLives == 0) {
      return $this->redirectToRoute('jelen_gamecode_index');
    }

    $player = $this->getDoctrine()->getRepository(Player::class)->find($session->get('playerId'));
    $code = $session->get('gameCode');

    $gameCode = $em->getRepository(GameCode::class)->findOneBy(['code' => $code]);
    $device = $this->getDevice();
    $ip = $request->getClientIp();
    $level = $request->request->has('level') ? $request->request->get('level') : 1;

    $Game = $em->getRepository(Game::class)->newGame($player, $gameCode, $device, $ip, $numOfLives, $level);

    $session->set('gameId', $Game->getId());

    return new JsonResponse(['success' => 1, 'hash' => $Game->getHashCode()]);

  }

  /**
   * @Route("/game-update", name="jelen_interakcije_game_update", options={"expose"=true})
   * @param Request $request
   * @param Session $session
   * @return JsonResponse|RedirectResponse
   */
  public function gameUpdate(Request $request, Session $session) {
    $em = $this->getDoctrine()->getManager();

//    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
//      return $this->redirectToRoute('jelen_default_login', [], 301);
//    }

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    $args = [];
    
    $game = $em->getRepository(Game::class)->find($session->get('gameId'));
    $game->setUpdated(new \DateTime());
    if ($request->request->has('level')) {
      $level = (int)$request->request->get('level') + 1;
      $game->setLevel($level);
    }

    if ($request->request->has('score')) {
      $game->setPoints($request->request->get('score'));
    }

    if ($request->request->has('levelScore')) {
      $game->setLevelPoints($request->request->get('levelScore'));
    }

    if ($request->request->has('saves')) {
      $game->setSaves($request->request->get('saves'));
    }

    if ($request->request->has('perfectSaves')) {
      $game->setPerfectSaves($request->request->get('perfectSaves'));
    }

    if ($request->request->has('goals')) {
      $game->setGoals($request->request->get('goals'));
    }

    $lives = $session->get('numOfLives');

    //match == 1 nije prosao nivo
    if ($request->request->has('match') && $request->request->get('match') == 1) {
      $lives = $lives - 1;
    }
    $session->set('numOfLives', $lives);

    $game->setLives($lives);

    $em->flush();
    
    $session->remove('gameId');


    if ($session->get('numOfLives') == 0) {
      $session->remove('gameCode');
      return new JsonResponse(['success' => -1, 'numOfLives' => $lives]);
    } else {
      return new JsonResponse([ 'success' => 1, 'numOfLives' => $lives ]);
    }
  }

  private function getDevice(): array {
//    $MobileDetector = $this->get('mobile_detect.mobile_detector');
//    $device['mobile'] = $MobileDetector->isMobile() ? true : false;
//    $device['tablet'] = $MobileDetector->isTablet() ? true : false;
//    $device['desktop'] = (!$device['mobile'] && !$device['tablet']) ? true : false;

    $device['mobile'] = false;
    $device['tablet'] = false;
    $device['desktop'] = true;

    return $device;
  }

  /**
   * @Route("/checkemail", name="jelen_interakcije_checkmail", options={"expose"=true})
   * @param Request $request
   * @return JsonResponse
   */
  public function checkEmail(Request $request): JsonResponse {
    $em = $this->getDoctrine()->getManager();
    $args = [];
    
    $email = $request->request->get('email');
    $User = $em->getRepository(Player::class)->findOneBy(['email' => $email]);
    if($User) {
      $args['success'] = 1;
    } else {
      $args['success'] = -1;
    }
    return new JsonResponse($args);
  }

  private function checkIfCodeIsUploaded($gameCode, $player): bool {
    $em = $this->getDoctrine()->getManager();

    $Code = $em->getRepository(Code::class)->findOneBy([ 'code' => $gameCode ]);

    if($Code) {
      $Code->setIsUsed(1);
      $Code->setPlayer($player);
      $Code->setUsed(new \DateTime());
      $em->flush();
    }

    return $Code ? true : false;
  }

}
