<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/update")
 */
class UpdateController extends AbstractController
{

  //CREATE TABLE `cron_table` ( `id` INT NOT NULL AUTO_INCREMENT , `file_name` VARCHAR(100) NOT NULL , `first_cron_completed` TINYINT NULL DEFAULT '0', PRIMARY KEY (`id`)) ENGINE = InnoDB;

  /**
   * @Route("/score", name="jelen_update_score")
   */
  public function score(): Response {
    $ScoreService = $this->get('jelen.score');
    $ScoreService->insert();

    return new Response('');
  }

  // Prvo se pokrece ovaj url pa tek onda url iznad
  /**
   * @Route("/valid-game-codes", name="jelen_update_valid_game_codes")
   */
  public function validGameCodes(): Response {
    $CodeService = $this->get('home.codes');
    $CodeService->activate();

    return new Response('');
  }
}
