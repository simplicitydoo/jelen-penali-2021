<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/admin/upload")
 */
class UploadController extends AbstractController
{
  /**
   * @Route("/", name="jelen_upload_codes")
   * @param Request $request
   * @return RedirectResponse
   */
  public function codes(Request $request): RedirectResponse {
    $em = $this->getDoctrine()->getManager();
    $logger = $this->get('logger');

    $Codes = $this->get('home.codes');

    $file = $request->files->get('codes');
    
    $res = $Codes->save($file);
    $logger->error('codes:: ', $res);
    
    if ($res['success'] == 1) {
      $this->addFlash('sonata_flash_success', 'Uspesno uploadovan fajl');
    } else if ($res['success'] == -1) {
      $this->addFlash('sonata_flash_error', 'Doslo je do greske prilikom uploada fajla. Pokusajte ponovo');
    }

    return $this->redirectToRoute('sonata_admin_dashboard');
  }

}