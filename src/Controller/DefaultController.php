<?php

namespace App\Controller;

use App\Entity\Player;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController {
  /**
   * @Route("/", name="jelen_default_index", options={"expose"=true})
   * @param Request $request
   * @param Session $session
   * @return Response
   */
  public function index(Request $request, Session $session): Response {
    $args = [];

//        $session->set('ageVerified', 1);

    if ($request->query->has('ageError')) {
      $args['ageError'] = -1;
    }

    return $this->render('Default/index.html.twig', $args);
  }

  /**
   * @Route("/pravila", name="jelen_default_rules")
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function rules(Session $session) {
    $args = [];

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    return $this->render('Default/rules.html.twig', $args);
  }

  /**
   * @Route("/nagrade", name="jelen_default_prizes")
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function prizes(Session $session) {
    $args = [];

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    return $this->render('Default/prizes.html.twig', $args);
  }

  /**
   * @Route("/registracija", name="jelen_default_registration")
   * @param Request $request
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function registration(Request $request, Session $session) {
    $args = [];

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    if ($request->query->has('registrationError')) {
      $args['registrationError'] = true;
    }

    return $this->render('Forms/index.html.twig', $args);
  }

  /**
   * @Route("/prijavi-se", name="jelen_default_login")
   * @param Request $request
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function login(Request $request, Session $session) {
    $args = [];

    if (!$session->get('ageVerified')) {
      $args['ageError'] = -1;
      return $this->redirectToRoute('jelen_default_index', $args, 301);
    }

    if ($request->getMethod() == 'POST') {
      $email = $request->request->get('_username');
      $pass = $request->request->get('_password');

      $player = $this->getDoctrine()->getRepository(Player::class)->findOneBy(['email' => $email]);

      if (!is_null($player) && password_verify($pass, $player->getPassword())) {
        $session->set('playerId', $player->getId());
        return $this->redirectToRoute('jelen_gamecode_index');
      } else {
        $args['error'] = true;
      }
    }

    return $this->render('Forms/login.html.twig', $args);
  }

}
