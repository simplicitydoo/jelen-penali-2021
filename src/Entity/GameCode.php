<?php

namespace App\Entity;

use App\Repository\GameCodeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="eo_game_code")
 * @ORM\Entity(repositoryClass=GameCodeRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class GameCode {
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Player")
   * @ORM\JoinColumn(name="fk_player_id", referencedColumnName="id")
   **/
  private $player;

  /**
   * @ORM\Column(name="code", type="string", length=30)
   */
  private $code;

  /**
   * @ORM\Column(name="ip", type="string", length=75)
   */
  private $ip;

  /**
   * @ORM\Column(name="is_valid", type="boolean")
   */
  private $isValid;

  /**
   * @ORM\Column(name="is_test", type="boolean")
   */
  private $isTest;

  /**
   * @ORM\Column(name="is_used", type="boolean")
   */
  private $isUsed;
  /**
   * @ORM\Column(name="is_mobile", type="boolean")
   */
  private $isMobile;

  /**
   * @ORM\Column(name="id_table", type="boolean")
   */
  private $isTablet;

  /**
   * @ORM\Column(name="is_desktop", type="boolean")
   */
  private $isDesktop;

  /**
   * @ORM\Column(name="user_agent", type="string", length=255)
   */
  private $userAgent;

  /**
   * @ORM\Column(name="created", type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime", nullable=true)
   */
  private $updated;

  /**
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  /**
   * @ORM\PreUpdate
   */
  public function preUpdate() {
    $this->updated = new \DateTime();
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getPlayer(): ?Player {
    return $this->player;
  }

  public function setPlayer(Player $player): self {
    $this->player = $player;

    return $this;
  }

  public function getCode(): ?string {
    return $this->code;
  }

  public function setCode(string $code): self {
    $this->code = $code;

    return $this;
  }

  public function getIp(): ?string {
    return $this->ip;
  }

  public function setIp(string $ip): self {
    $this->ip = $ip;

    return $this;
  }

  public function getIsValid(): ?bool {
    return $this->isValid;
  }

  public function setIsValid(bool $isValid): self {
    $this->isValid = $isValid;

    return $this;
  }

  public function getIsTest(): bool {
    return $this->isTest;
  }

  public function setIsTest(bool $isTest): void {
    $this->isTest = $isTest;
  }

  public function getIsUsed(): bool {
    return $this->isUsed;
  }

  public function setIsUsed(bool $isUsed): void {
    $this->isUsed = $isUsed;
  }

  public function getIsMobile(): ?bool {
    return $this->isMobile;
  }

  public function setIsMobile(bool $isMobile): self {
    $this->isMobile = $isMobile;

    return $this;
  }

  public function getIsTablet(): ?bool {
    return $this->isTablet;
  }

  public function setIsTablet(bool $isTablet): self {
    $this->isTablet = $isTablet;

    return $this;
  }

  public function getIsDesktop(): ?bool {
    return $this->isDesktop;
  }

  public function setIsDesktop(bool $isDesktop): self {
    $this->isDesktop = $isDesktop;

    return $this;
  }

  public function getUserAgent(): ?string {
    return $this->userAgent;
  }

  public function setUserAgent(string $userAgent): self {
    $this->userAgent = $userAgent;

    return $this;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(?\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }

  public function getUpdated(): ?\DateTimeInterface {
    return $this->updated;
  }

  public function setUpdated(?\DateTimeInterface $updated): self {
    $this->updated = $updated;

    return $this;
  }
}
