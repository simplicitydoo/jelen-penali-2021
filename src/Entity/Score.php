<?php

namespace App\Entity;

use App\Repository\ScoreRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="eo_score")
 * @ORM\Entity(repositoryClass=ScoreRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Score {
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Player")
   * @ORM\JoinColumn(name="fk_player_id", referencedColumnName="id")
   **/
  private $player;

  /**
   * @ORM\OneToOne(targetEntity="GameCode")
   * @ORM\JoinColumn(name="fk_game_code_id", referencedColumnName="id")
   **/
  private $gameCode;

  /**
   * @ORM\Column(name="points", type="integer")
   */
  private $points;

  /**
   * @ORM\Column(name="level", type="integer")
   */
  private $level;

  /**
   * @ORM\Column(name="saves", type="integer")
   */
  private $saves;

  /**
   * @ORM\Column(name="perfect_saves", type="integer")
   */
  private $perfectSaves;

  /**
   * @ORM\Column(name="goals", type="integer", nullable=true)
   */
  private $goals;

  /**
   * @ORM\Column(name="lives", type="smallint", nullable=true)
   */
  private $lives;

  /**
   * @ORM\Column(name="created", type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime", nullable=true)
   */
  private $updated;

  /**
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  /**
   * @ORM\PreUpdate
   */
  public function preUpdate() {
    $this->updated = new \DateTime();
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getPlayer(): ?Player {
    return $this->player;
  }

  public function setPlayer(Player $player): self {
    $this->player = $player;

    return $this;
  }

  public function getGameCode(): ?GameCode {
    return $this->gameCode;
  }

  public function setGameCode(GameCode $gameCode): self {
    $this->gameCode = $gameCode;

    return $this;
  }

  public function getPoints(): ?int {
    return $this->points;
  }

  public function setPoints(int $points): self {
    $this->points = $points;

    return $this;
  }

  public function getLevel(): ?int {
    return $this->level;
  }

  public function setLevel(int $level): self {
    $this->level = $level;

    return $this;
  }

  public function getSaves(): ?int {
    return $this->saves;
  }

  public function setSaves(int $saves): self {
    $this->saves = $saves;

    return $this;
  }

  public function getPerfectSaves(): ?int {
    return $this->perfectSaves;
  }

  public function setPerfectSaves(int $perfectSaves): self {
    $this->perfectSaves = $perfectSaves;

    return $this;
  }

  public function getGoals(): ?int {
    return $this->goals;
  }

  public function setGoals(?int $goals): self {
    $this->goals = $goals;

    return $this;
  }

  public function getLives(): ?int {
    return $this->lives;
  }

  public function setLives(?int $lives): self {
    $this->lives = $lives;

    return $this;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(?\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }

  public function getUpdated(): ?\DateTimeInterface {
    return $this->updated;
  }

  public function setUpdated(?\DateTimeInterface $updated): self {
    $this->updated = $updated;

    return $this;
  }
}
