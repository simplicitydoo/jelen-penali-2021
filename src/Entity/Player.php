<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="players", indexes={@ORM\Index(name="phone", columns={"phone"})})
 * @ORM\Entity(repositoryClass=PlayerRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Player implements UserInterface {
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(name="first_name", type="string", length=50, nullable=true)
   */
  private $firstName;

  /**
   * @ORM\Column(name="last_name", type="string", length=50, nullable=true)
   */
  private $lastName;

  /**
   * @ORM\Column(name="phone", type="string", length=50, nullable=true)
   */
  private $phone;

  /**
   * @ORM\Column(name="email", type="string", length=100, nullable=true)
   */
  private $email;

  /**
   * The encoded password
   *
   * @ORM\Column(name="password", type="string", length=255, nullable=true)
   */
  private $password;

  /**
   * A non-persisted field that's used to create the encoded password.
   *
   * @var string
   */
  private $plainPassword;

  /**
   * @ORM\Column(name="city", type="string", length=128, nullable=true)
   */
  private $city;

  /**
   * @ORM\Column(name="zip", type="string", length=20, nullable=true)
   */
  private $zip;

  /**
   * @ORM\Column(name="address", type="string", length=255, nullable=true)
   */
  private $address;

  /**
   * @ORM\Column(name="code", type="string", length=50, nullable=true)
   */
  private $code;

  /**
   * @ORM\Column(name="accept_rules", type="boolean")
   */
  private $acceptRules = false;

  /**
   * @ORM\Column(name="no_of_codes", type="integer")
   */
  private $noOfCodes = 0;

  /**
   * @ORM\Column(name="enabled", type="boolean")
   */
  private $enabled = false;

  /**
   * @ORM\Column(name="created", type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime", nullable=true)
   */
  private $updated;

  /**
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  /**
   * @ORM\PreUpdate
   */
  public function preUpdate() {
    $this->updated = new \DateTime();
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getFirstName(): ?string {
    return $this->firstName;
  }

  public function setFirstName(string $firstName): self {
    $this->firstName = $firstName;

    return $this;
  }

  public function getLastName(): ?string {
    return $this->lastName;
  }

  public function setLastName(string $lastName): self {
    $this->lastName = $lastName;

    return $this;
  }

  public function getPhone(): ?string {
    return $this->phone;
  }

  public function setPhone(string $phone): self {
    $this->phone = $phone;

    return $this;
  }

  public function getEmail(): ?string {
    return $this->email;
  }

  public function setEmail(string $email): self {
    $this->email = $email;

    return $this;
  }

  public function getCity(): ?string {
    return $this->city;
  }

  public function setCity(string $city): self {
    $this->city = $city;

    return $this;
  }

  public function getZip(): ?string {
    return $this->zip;
  }

  public function setZip($zip): self {
    $this->zip = $zip;

    return $this;
  }

  public function getAddress(): ?string {
    return $this->address;
  }

  public function setAddress(string $address): self {
    $this->address = $address;

    return $this;
  }

  public function getCode(): ?string {
    return $this->code;
  }

  public function setCode(string $code): self {
    $this->code = $code;

    return $this;
  }

  public function getAcceptRules(): ?bool {
    return $this->acceptRules;
  }

  public function setAcceptRules(bool $acceptRules): self {
    $this->acceptRules = $acceptRules;

    return $this;
  }

  public function getNoOfCodes(): ?bool {
    return $this->noOfCodes;
  }

  public function setNoOfCodes(bool $noOfCodes): self {
    $this->noOfCodes = $noOfCodes;

    return $this;
  }

  public function isEnabled(): bool {
    return $this->enabled;
  }

  public function setEnabled(bool $enabled): void {
    $this->enabled = $enabled;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }

  public function getUpdated(): ?\DateTimeInterface {
    return $this->updated;
  }

  public function setUpdated(\DateTimeInterface $updated): self {
    $this->updated = $updated;

    return $this;
  }

  public function getRoles() {
    // TODO: Implement getRoles() method.
  }

  public function getPassword(): ?string {
    // TODO: Implement getPassword() method.
    return $this->password;
  }

  public function setPassword(string $password): void {
    $this->password = $password;
  }

  public function getSalt() {
    // TODO: Implement getSalt() method.
  }

  public function getUsername() {
    // TODO: Implement getUsername() method.
  }

  public function eraseCredentials() {
    $this->plainPassword = null;
  }

  public function getPlainPassword(): string {
    return $this->plainPassword;
  }

  public function setPlainPassword(string $plainPassword): void {
    $this->plainPassword = $plainPassword;
    $this->password = null;
  }


}
