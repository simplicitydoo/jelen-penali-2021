<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\GameCode;

/**
 * @ORM\Table(name="eo_game")
 * @ORM\Entity(repositoryClass=GameRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Game {
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Player")
   * @ORM\JoinColumn(name="fk_player_id", referencedColumnName="id")
   **/
  private $player;

  /**
   * @ORM\ManyToOne(targetEntity="GameCode")
   * @ORM\JoinColumn(name="fk_game_code_id", referencedColumnName="id")
   **/
  private $gameCode;

  /**
   * @ORM\Column(name="level_points", type="integer", nullable=true)
   */
  private $levelPoints;

  /**
   * @ORM\Column(name="points", type="integer", nullable=true)
   */
  private $points;

  /**
   * @ORM\Column(name="saves", type="integer", nullable=true)
   */
  private $saves;

  /**
   * @ORM\Column(name="perfect_saves", type="integer", nullable=true)
   */
  private $perfectSaves;

  /**
   * @ORM\Column(name="goals", type="integer", nullable=true)
   */
  private $goals;

  /**
   * @ORM\Column(name="level", type="integer", nullable=true)
   */
  private $level;

  /**
   * @ORM\Column(name="is_mobile", type="boolean")
   */
  private $isMobile;

  /**
   * @ORM\Column(name="is_tablet", type="boolean")
   */
  private $isTablet;

  /**
   * @ORM\Column(name="is_desktop", type="boolean")
   */
  private $isDesktop;

  /**
   * @ORM\Column(name="ip", type="string", length=75)
   */
  private $ip;

  /**
   * @ORM\Column(name="is_code_valid", type="boolean")
   */
  private $isCodeValid;

  /**
   * @ORM\Column(name="lives", type="smallint", nullable=true)
   */
  private $lives;

  /**
   * @ORM\Column(name="hash_code", type="string", length=255)
   */
  private $hashCode;

  /**
   * @ORM\Column(name="server_start_time", type="datetime", nullable=true)
   */
  private $serverStartTime;

  /**
   * @ORM\Column(name="server_end_time", type="datetime", nullable=true)
   */
  private $serverEndTime;

  /**
   * @ORM\Column(name="created", type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime", nullable=true)
   */
  private $updated;

  /**
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  /**
   * @ORM\PreUpdate
   */
  public function preUpdate() {
    $this->updated = new \DateTime();
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getPlayer(): ?Player {
    return $this->player;
  }

  public function setPlayer(Player $player): self {
    $this->player = $player;

    return $this;
  }

  public function getGameCode(): ?GameCode {
    return $this->gameCode;
  }

  public function setGameCode(GameCode $gameCode): self {
    $this->gameCode = $gameCode;

    return $this;
  }

  public function getLevelPoints(): ?int {
    return $this->levelPoints;
  }

  public function setLevelPoints(?int $levelPoints): self {
    $this->levelPoints = $levelPoints;

    return $this;
  }

  public function getPoints(): ?int {
    return $this->points;
  }

  public function setPoints(?int $points): self {
    $this->points = $points;

    return $this;
  }

  public function getSaves(): ?int {
    return $this->saves;
  }

  public function setSaves(?int $saves): self {
    $this->saves = $saves;

    return $this;
  }

  public function getPerfectSaves(): ?int {
    return $this->perfectSaves;
  }

  public function setPerfectSaves(?int $perfectSaves): self {
    $this->perfectSaves = $perfectSaves;

    return $this;
  }

  public function getGoals(): ?int {
    return $this->goals;
  }

  public function setGoals(?int $goals): self {
    $this->goals = $goals;

    return $this;
  }

  public function getLevel(): ?int {
    return $this->level;
  }

  public function setLevel(?int $level): self {
    $this->level = $level;

    return $this;
  }

  public function getIsMobile(): ?bool {
    return $this->isMobile;
  }

  public function setIsMobile(bool $isMobile): self {
    $this->isMobile = $isMobile;

    return $this;
  }

  public function getIsTablet(): ?bool {
    return $this->isTablet;
  }

  public function setIsTablet(bool $isTablet): self {
    $this->isTablet = $isTablet;

    return $this;
  }

  public function getIsDesktop(): ?bool {
    return $this->isDesktop;
  }

  public function setIsDesktop(bool $isDesktop): self {
    $this->isDesktop = $isDesktop;

    return $this;
  }

  public function getIp(): ?string {
    return $this->ip;
  }

  public function setIp(string $ip): self {
    $this->ip = $ip;

    return $this;
  }

  public function getIsCodeValid(): ?bool {
    return $this->isCodeValid;
  }

  public function setIsCodeValid(bool $isCodeValid): self {
    $this->isCodeValid = $isCodeValid;

    return $this;
  }

  public function getLives(): ?int {
    return $this->lives;
  }

  public function setLives(?int $lives): self {
    $this->lives = $lives;

    return $this;
  }

  public function getHashCode(): ?string {
    return $this->hashCode;
  }

  public function setHashCode(string $hashCode): self {
    $this->hashCode = $hashCode;

    return $this;
  }

  public function getServerStartTime(): ?\DateTime {
    return $this->serverStartTime;
  }

  public function setServerStartTime(?\DateTime $serverStartTime): void {
    $this->serverStartTime = $serverStartTime;
  }

  public function getServerEndTime(): ?\DateTime {
    return $this->serverEndTime;
  }

  public function setServerEndTime(?\DateTime $serverEndTime): void {
    $this->serverEndTime = $serverEndTime;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(?\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }

  public function getUpdated(): ?\DateTimeInterface {
    return $this->updated;
  }

  public function setUpdated(?\DateTimeInterface $updated): self {
    $this->updated = $updated;

    return $this;
  }
}
