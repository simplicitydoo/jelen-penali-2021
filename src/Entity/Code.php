<?php

namespace App\Entity;

use App\Repository\CodeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="eo_code")
 * @ORM\Entity(repositoryClass=CodeRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Code {
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(name="code", type="string", length=25)
   */
  private $code;

  /**
   * @ORM\ManyToOne(targetEntity="Player")
   * @ORM\JoinColumn(name="fk_player_id", referencedColumnName="id")
   **/
  private $player;

  /**
   * @ORM\Column(name="is_used", type="boolean")
   */
  private $isUsed;

  /**
   * @ORM\Column(name="used", type="datetime", nullable=true)
   */
  private $used;

  /**
   * @ORM\Column(name="created", type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime", nullable=true)
   */
  private $updated;

  /**
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  /**
   * @ORM\PreUpdate
   */
  public function preUpdate() {
    $this->updated = new \DateTime();
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getCode(): ?string {
    return $this->code;
  }

  public function setCode(string $code): self {
    $this->code = $code;

    return $this;
  }

  public function getPlayer(): ?Player {
    return $this->player;
  }

  public function setPlayer(Player $player): self {
    $this->player = $player;

    return $this;
  }

  public function getIsUsed(): ?bool {
    return $this->isUsed;
  }

  public function setIsUsed(bool $isUsed): self {
    $this->isUsed = $isUsed;

    return $this;
  }

  public function getUsed(): ?\DateTimeInterface {
    return $this->used;
  }

  public function setUsed(?\DateTimeInterface $used): self {
    $this->used = $used;

    return $this;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(?\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }

  public function getUpdated(): ?\DateTimeInterface {
    return $this->updated;
  }

  public function setUpdated(?\DateTimeInterface $updated): self {
    $this->updated = $updated;

    return $this;
  }
}
