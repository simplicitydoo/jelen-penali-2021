<?php

namespace App\Twig;

use App\Entity\Score;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension {

  private $em;

  public function __construct(EntityManagerInterface $em) {
    $this->em = $em;
  }

  public function getFilters() {
    return [

    ];
  }

  public function getFunctions() {
    return array(
      new TwigFunction('getenv', [$this, 'getenv']),
      new TwigFunction('getScoreData', [$this, 'getScoreData']),
    );
  }


  public function getenv(string $name, string $type = '') {
    if ($type == 'int') {
      return (int)$_ENV[$name];
    }
    if ($type == 'float') {
      return (float)$_ENV[$name];
    }

    return $_ENV[$name];
  }

  public function getScoreData(string $playerId, string $points): Score {
    return $this->em->getRepository(Score::class)->findOneBy(['player' => $playerId, 'points' => $points]);
  }

}
