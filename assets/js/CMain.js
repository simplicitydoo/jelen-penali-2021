function CMain(oData) {
    var _bUpdate;
    var _iCurResource = 0;
    var RESOURCE_TO_LOAD = 0;
    var _iState = STATE_LOADING;
    var _oData;

    var _oPreloader;
    var _oMenu;
    var _oHelp;
    var _oLevelMenu;
    var _oGame;

    this.initContainer = function () {
        var canvas = document.getElementById("canvas");
        s_oStage = new createjs.Stage(canvas);
        createjs.Touch.enable(s_oStage);
        s_oStage.preventSelection = false;

        s_bMobile = jQuery.browser.mobile;
        if (s_bMobile === false) {
            s_oStage.enableMouseOver(20);
            $('body').on('contextmenu', '#canvas', function (e) {
                return false;
            });
            FPS = FPS_DESKTOP;
            FPS_TIME = 1 / FPS;
            PHYSICS_STEP = 1 / (FPS * STEP_RATE);
            ROLL_BALL_RATE = 60 / FPS;
        } else {
            BALL_VELOCITY_MULTIPLIER = 0.8;
        }

        s_iPrevTime = new Date().getTime();

        createjs.Ticker.addEventListener("tick", this._update);
        createjs.Ticker.setFPS(FPS);

        if (navigator.userAgent.match(/Windows Phone/i)) {
            DISABLE_SOUND_MOBILE = true;
        }

        s_oSpriteLibrary = new CSpriteLibrary();

        //ADD PRELOADER
        _oPreloader = new CPreloader();

        _bUpdate = true;
    };

    this.soundLoaded = function () {
        _iCurResource++;
        var iPerc = Math.floor(_iCurResource / RESOURCE_TO_LOAD * 100);
        _oPreloader.refreshLoader(iPerc);

        if (_iCurResource === RESOURCE_TO_LOAD) {
            this._onRemovePreloader();
        }
    };
    
    this._initSounds = function(){
        var aSoundsInfo = new Array();
        aSoundsInfo.push({path: '/assets/sounds/',filename:'drop_bounce_grass',loop:false,volume:1, ingamename: 'drop_bounce_grass'});
        aSoundsInfo.push({path: '/assets/sounds/',filename:'click',loop:false,volume:1, ingamename: 'click'});
        aSoundsInfo.push({path: '/assets/sounds/',filename:'goal',loop:false,volume:1, ingamename: 'goal'});
        aSoundsInfo.push({path: '/assets/sounds/',filename:'keep_ball',loop:false,volume:1, ingamename: 'keep_ball'});
        aSoundsInfo.push({path: '/assets/sounds/',filename:'kick',loop:false,volume:1, ingamename: 'kick'});
        aSoundsInfo.push({path: '/assets/sounds/',filename:'win',loop:false,volume:1, ingamename: 'win'});
        aSoundsInfo.push({path: '/assets/sounds/',filename:'soundtrack',loop:true,volume:1, ingamename: 'soundtrack'});
        
        RESOURCE_TO_LOAD += aSoundsInfo.length;

        s_aSounds = new Array();
        for(var i=0; i<aSoundsInfo.length; i++){
            s_aSounds[aSoundsInfo[i].ingamename] = new Howl({ 
                                                            src: [aSoundsInfo[i].path+aSoundsInfo[i].filename+'.mp3', aSoundsInfo[i].path+aSoundsInfo[i].filename+'.ogg'],
                                                            autoplay: false,
                                                            preload: true,
                                                            loop: aSoundsInfo[i].loop, 
                                                            volume: aSoundsInfo[i].volume,
                                                            onload: s_oMain.soundLoaded()
                                                        });
        }
        
    };  

    this._loadImages = function () {
        s_oSpriteLibrary.init(this._onImagesLoaded, this._onAllImagesLoaded, this);

        s_oSpriteLibrary.addSprite("but_play", "/assets/images/sprites/but_play.png");
        s_oSpriteLibrary.addSprite("but_exit", "/assets/images/sprites/but_exit.png");
        s_oSpriteLibrary.addSprite("but_restart_small", "/assets/images/sprites/but_restart_small.png");
        s_oSpriteLibrary.addSprite("bg_menu", "/assets/images/sprites/bg_menu.jpg");
        s_oSpriteLibrary.addSprite("bg_game", "/assets/images/sprites/bg_game.jpg");
        s_oSpriteLibrary.addSprite("msg_box", "/assets/images/sprites/msg_box.png");
        s_oSpriteLibrary.addSprite("msg_box_small", "/assets/images/sprites/msg_box_small.png");
        s_oSpriteLibrary.addSprite("audio_icon", "/assets/images/sprites/audio_icon.png");
        s_oSpriteLibrary.addSprite("but_home", "/assets/images/sprites/but_home.png");
        s_oSpriteLibrary.addSprite("but_restart", "/assets/images/sprites/but_restart.png");
        s_oSpriteLibrary.addSprite("but_continue_big", "/assets/images/sprites/but_continue_big.png");
        s_oSpriteLibrary.addSprite("ball", "/assets/images/sprites/ball.png");
        s_oSpriteLibrary.addSprite("but_level", "/assets/images/sprites/but_level.png");
        s_oSpriteLibrary.addSprite("bg_game", "/assets/images/sprites/bg_game.jpg");
        s_oSpriteLibrary.addSprite("gloves", "/assets/images/sprites/gloves.png");
        s_oSpriteLibrary.addSprite("goal", "/assets/images/sprites/goal.png");
        s_oSpriteLibrary.addSprite("but_continue", "/assets/images/sprites/but_continue.png");
        s_oSpriteLibrary.addSprite("but_yes", "/assets/images/sprites/but_yes.png");
        s_oSpriteLibrary.addSprite("but_no", "/assets/images/sprites/but_no.png");
        s_oSpriteLibrary.addSprite("but_info", "/assets/images/sprites/but_info.png");
        s_oSpriteLibrary.addSprite("logo_ctl", "/assets/images/sprites/logo_ctl.png");
        s_oSpriteLibrary.addSprite("but_pause", "/assets/images/sprites/but_pause.png");
        s_oSpriteLibrary.addSprite("arrow_right", "/assets/images/sprites/arrow_right.png");
        s_oSpriteLibrary.addSprite("arrow_left", "/assets/images/sprites/arrow_left.png");
        s_oSpriteLibrary.addSprite("opponent", "/assets/images/sprites/opponent.png");
        s_oSpriteLibrary.addSprite("ball_shadow", "/assets/images/sprites/ball_shadow.png");
        s_oSpriteLibrary.addSprite("gui_panel", "/assets/images/sprites/gui_panel.png");
        s_oSpriteLibrary.addSprite("help_mouse", "/assets/images/sprites/help_mouse.png");
        s_oSpriteLibrary.addSprite("help_touch", "/assets/images/sprites/help_touch.png");
        s_oSpriteLibrary.addSprite("but_fullscreen", "/assets/images/sprites/but_fullscreen.png");
        s_oSpriteLibrary.addSprite("but_scores", "/assets/images/sprites/but_scores.png");


        RESOURCE_TO_LOAD += s_oSpriteLibrary.getNumSprites();
        s_oSpriteLibrary.loadSprites();
    };

    this._onImagesLoaded = function () {
        _iCurResource++;
        var iPerc = Math.floor(_iCurResource / RESOURCE_TO_LOAD * 100);
        _oPreloader.refreshLoader(iPerc);
        if (_iCurResource === RESOURCE_TO_LOAD) {
            this._onRemovePreloader();
        }
    };

    this._onAllImagesLoaded = function () {

    };

    this.onAllPreloaderImagesLoaded = function () {
        this._loadImages();
    };

    this.preloaderReady = function () {
        this._loadImages();
        if (DISABLE_SOUND_MOBILE === false || s_bMobile === false) {
            this._initSounds();
        }

        
        _bUpdate = true;
    };
    
    this._onRemovePreloader = function(){
        try{
            saveItem("ls_available","ok");
        }catch(evt){
            // localStorage not defined
            s_bStorageAvailable = false;
        }

        _oPreloader.unload();

        if (!isIOS()) {
            s_oSoundTrack = playSound("soundtrack", 1, true);
        }
        

        this.gotoMenu();
    };
    
    this.gotoMenu = function () {
        _oMenu = new CMenu(oData.ball_to_saved.length);
        _iState = STATE_MENU;
    };

    this.gotoGame = function (iLevel) {
        _oGame = new CGame(_oData, iLevel);

        _iState = STATE_GAME;
    };

    this.gotoLevelMenu = function () {
        _oLevelMenu = new CLevelMenu(oData.ball_to_saved.length);
        _iState = STATE_MENU;
    };

    this.gotoHelp = function () {
        _oHelp = new CHelp();
        _iState = STATE_HELP;
    };

    this.stopUpdate = function(){
        _bUpdate = false;
        createjs.Ticker.paused = true;
        $("#block_game").css("display","block");
        
        if(DISABLE_SOUND_MOBILE === false || s_bMobile === false){
            Howler.mute(true);
        }
        
    };

    this.startUpdate = function(){
        s_iPrevTime = new Date().getTime();
        _bUpdate = true;
        createjs.Ticker.paused = false;
        $("#block_game").css("display","none");
        
        if(DISABLE_SOUND_MOBILE === false || s_bMobile === false){
            if(s_bAudioActive){
                Howler.mute(false);
            }
        }
        
    };

    this._update = function (event) {
        if (_bUpdate === false) {
            return;
        }
        var iCurTime = new Date().getTime();
        s_iTimeElaps = iCurTime - s_iPrevTime;
        s_iCntTime += s_iTimeElaps;
        s_iCntFps++;
        s_iPrevTime = iCurTime;

        if (s_iCntTime >= 1000) {
            s_iCurFps = s_iCntFps;
            s_iCntTime -= 1000;
            s_iCntFps = 0;
        }

        if (_iState === STATE_GAME) {
            _oGame.update();
        }

        s_oStage.update(event);

    };

    this.endGame = function () {
        console.log(Routing.generate('jelen_score_index'));
        //window.location.href = window.location.origin  + '/rang-lista';
        window.location.href = Routing.generate('jelen_score_index');
    }

    s_oMain = this;

    _oData = oData;
    ENABLE_FULLSCREEN = oData.fullscreen;
    ENABLE_CHECK_ORIENTATION = oData.check_orientation;
    
    this.initContainer();
}
var s_bMobile;
var s_bAudioActive = true;
var s_bFullscreen = false;
var s_bStorageAvailable = true;
var s_iCntTime = 0;
var s_iTimeElaps = 0;
var s_iPrevTime = 0;
var s_iCntFps = 0;
var s_iCurFps = 0;
var s_oPhysicsController;
var s_iLevelReached = 18;
var s_iCanvasResizeHeight;
var s_iCanvasResizeWidth;
var s_iCanvasOffsetHeight;
var s_iCanvasOffsetWidth;
var s_iAdsLevel = 1;
var s_iLastLevel = 1;
var s_aScores = new Array();

var s_oDrawLayer;
var s_oStage;
var s_oMain;
var s_oSpriteLibrary;
var s_oSoundTrack = null;
var s_aSounds;