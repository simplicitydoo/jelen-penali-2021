var gift_click = false;
$(function(){

  $.extend($.validator.messages, {
    required: "Ovo polje je obavezno.",
    email: "Unesite ispravnu e-mail adresu.",
    number: "Unesite ispravan broj.",
    digits: "Unesite samo brojeve.",
  });

  var ageVerifyBtn = $('#submitPlay');

  //init select2
  if ($('.age-check').length > 0) {
    $('#day').select2();
    $('#month').select2();
    $('#year').select2();
  }

  ageVerifyBtn.on('click', function(e) {
    e.preventDefault();
    $('#minor-age').hide();
    var day = $('#day').val();
    var month = $('#month').val();
    var year = parseInt($('#year').val()) + 18;
    var myDate = new Date();
    myDate.setFullYear(year, month - 1, day);
    if (new Date(year, month - 1, day) <= new Date()) {
      $.post(Routing.generate('jelen_interakcije_age_verify'), { 'ageVerified': 1 }, function (res) {
        window.location.href = Routing.generate('jelen_default_index');
      });
    } else {
      $('#minor-age').show();
    }
  });

  if ($('#game').length > 0) {
    localStorage.clear();
    var oMain = new CMain({
      ball_to_saved: [3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
      ball_force_left_right: [5, 6, 7, 10, 10, 12, 10, 12, 10, 12], //DIRECTION BALL FORCE LEFT RIGHT
      ball_force_up: [{ min: 6, max: 9 }, { min: 5, max: 9 }, { min: 3, max: 9 }, { min: 1, max: 8 }, { min: 2, max: 8 }, { min: 4, max: 8 },
      { min: 3, max: 9 }, { min: 1, max: 10 }, { min: 5, max: 8 }, { min: 5, max: 10.5 }], //DIRECTION BALL FORCE UP
      ball_force_velocity: [30, 40, 50, 60, 65, 65, 70, 80, 80, 90], //BALL FORCE VELOCITY
      max_launch: [5, 5, 6, 5, 5, 6, 6, 6, 5, 6], //MAX MATCH BALL LAUNCH 
      score_perfect_ball_saved: 10, //ADD SCRORE WHEN THE GUANTES SAVE BALL LOCKED
      score_ball_saved: 5, //ADD SCRORE WHEN THE GUANTES SAVE BALL
      score_goal_opponent: -5, // ADD SCRORE WHEN THE OPPONENT GOAL
      perfect_ball_saved_point: { x: 0.7, z: 0.6 }, //POINT COLLISION FOR LOCKED BALL IN HANDS
      fullscreen: true,        //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
      check_orientation: true, //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES                     
      num_levels_for_ads: 2//NUMBER OF TURNS PLAYED BEFORE AD SHOWING //
      //////// THIS FEATURE  IS ACTIVATED ONLY WITH CTL ARCADE PLUGIN./////////////////////////// 
      /////////////////// YOU CAN GET IT AT: ///////////////////////////////////////////////////////// 
      // http://codecanyon.net/item/ctl-arcade-wordpress-plugin/13856421 ///////////
    });

    var ws = new WebSocket(CMS.data.wsSrv);
    var hash;

    $(oMain).on("start_session", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeStartSession();
      }   
      $.post(Routing.generate('jelen_interakcije_game_start'), function (res) {
        if (res.success == 1) {
          hash = res.hash;
          ws.send('hash-' + hash);
          ws.send('start');
        }
      });
    });

    $(oMain).on("end_session", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeEndSession();
      }
    });

    $(oMain).on("start_level", function (evt, iLevel) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeStartLevel({ level: iLevel });
      }
      $.post(Routing.generate('jelen_interakcije_game_start'), function (res) {
        if (res.success == 1) {
          hash = res.hash;
          ws.send('hash-' + hash);
          ws.send('start');
        }
      });
    });

    $(oMain).on("restart_level", function (evt, iLevel) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeRestartLevel({ level: iLevel });
      }
      $.post(Routing.generate('jelen_interakcije_game_start'), {'level' : iLevel}, function (res) {
      });
    });

    $(oMain).on("end_level", function (evt, iLevel, iScore) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeEndLevel({ level: iLevel });
      }
      ws.send('hash-' + hash);
      ws.send('end--')
    });

    $(oMain).on("save_score", function (evt, iScore, szMode) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeSaveScore({ score: iScore, mode: szMode });
      }
    });

    $(oMain).on("show_interlevel_ad", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeShowInterlevelAD();
      }
    });

    $(oMain).on("share_event", function (evt, iScore) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeShareEvent({
          img: TEXT_SHARE_IMAGE,
          title: TEXT_SHARE_TITLE,
          msg: TEXT_SHARE_MSG1 + iScore + TEXT_SHARE_MSG2,
          msg_share: TEXT_SHARE_SHARE1 + iScore + TEXT_SHARE_SHARE1
        });
      }
    });

    if (isIOS()) {
      setTimeout(function () {
        sizeHandler();
      }, 200);
    } else {
      sizeHandler();
    }
  }

  $('#user-registration-form').validate();

  $('#login-form').validate();
  
  $('#code-validation-form').validate();

  $('.mobile-menu-btn').click(function(e){
    e.preventDefault();
    $('.main-navigation').slideToggle();
  });

  $('.close').on('click', function() {
    $('#privacyModal').fadeOut('slow', function() {
      $('#ageBox').slideDown('slow');
      setCookie('ageBoxRead', true, 30);
    });
    $('#notificationModal').hide();
  });

  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  var loginUserBtn = $('#loginUserBtn');
  
  loginUserBtn.on('click', function(e) {
    e.preventDefault();
    $('.no-emil-error-msg').hide();
    var email = $('#email').val();
    $.post(Routing.generate('jelen_interakcije_checkmail') , {'email' : email}, function(res) {
      if(res.success == 1) {
        $('#login-form').submit();
      } else {
        $('.no-emil-error-msg').show();
      }
    })
  });

  $('#closeNotificationModalBtn').on('click', function (e) {
      e.preventDefault();
      $('#notificationModal').hide();
  });

});
